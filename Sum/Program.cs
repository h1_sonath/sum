﻿using System;

namespace Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            double Sum = 0;
            Console.WriteLine("Nhap so N: ");
            double numberN = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so X: ");
            int numberX = Convert.ToInt32(Console.ReadLine());
            
            for(int i = 1; i <= numberN; i++)
            {
                Sum = Sum + Factorial(i)/(Math.Pow(numberX, i ));
            }
            Console.WriteLine(Sum + 1);
        }
        public static double Factorial(double n)
        {

            if (n == 0 || n == 1)
            {
                return 1;
            }
            else return n * Factorial(n - 1);
        }
    }
}
